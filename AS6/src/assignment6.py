import numpy as np
import cv2
import sys
import as6 as assg
from scipy.ndimage.filters import gaussian_filter

#import PA2_1 as hornS
cap = cv2.VideoCapture('Walk2.mpg')
# params for ShiTomasi corner detection
feature_params = dict( maxCorners = 100,
                       qualityLevel = 0.3,
                       minDistance = 7,
                       blockSize = 7 )
# Parameters for lucas kanade optical flow
lk_params = dict( winSize  = (15,15),
                  maxLevel = 2,
                  criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
# Create some random colors
color = np.random.randint(0,255,(100,3))
# Take first frame and find corners in it
ret, old_frame = cap.read()
old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
p0 = cv2.goodFeaturesToTrack(old_gray, mask = None, **feature_params)
p0=p0.reshape(63,2)
# Create a mask image for drawing purposes
mask = np.zeros_like(old_frame)
kernel = 5
while(1):
    ret,frame = cap.read()
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # calculate optical flow
    Y,X,Z = frame.shape
    POI = assg.getPOI(X,Y,kernel)
    W = assg.gaussianWeight(kernel)
    old_gray = gaussian_filter(old_gray, 7)
    p1,st = assg.HornSchunck(old_gray, frame_gray)
    
    good_new =p1
    good_old = p0
    good_new=(np.abs(good_new.reshape(55296,2))).astype(np.float16)
    good_old1=(np.abs(good_old.reshape(63,2).astype(np.float16)))
    # draw the tracks
    for i,(new,old) in enumerate(zip(good_new,good_old1)):
        a,b = new.ravel()
        c,d = old.ravel()
        mask = cv2.line(mask, (a,b),(c,d), color[i].tolist(), 2)
        frame = cv2.circle(frame,(a,b),5,color[i].tolist(),-1)
    img = cv2.add(frame,mask)
    cv2.imshow('frame',img)
    k = cv2.waitKey(10) & 0xff
    if k == 27:
        break
    if cv2.waitKey(10) == ord('p'):
        while True:
            if cv2.waitKey(10) == ord('p'):
                break
            if cv2.waitKey(10) == 27:
                cv2.destroyAllWindows()
                cap.release()
                sys.exit()

    old_gray = frame_gray.copy()
#    p0 = good_new.reshape(-1,1,2)
cv2.destroyAllWindows()
cap.release()