import numpy as np
from scipy.ndimage.filters import convolve as filter2

HSKERN =np.array([[1/12, 1/6, 1/12],
                  [1/6,    0, 1/6],
                  [1/12, 1/6, 1/12]],float)

kernelX = np.array([[-1, 1],
                     [-1, 1]]) * .25 #kernel for computing d/dx
kernelY = np.array([[-1,-1],
                     [ 1, 1]]) * .25 #kernel for computing d/dy
kernelT = np.ones((2,2))*.25

def HornSchunck(im1, im2):

    alpha=0.001 
    Niter=8
    im1 = im1.astype(np.float32)
    im2 = im2.astype(np.float32)
  

	
    uInitial = np.zeros([im1.shape[0],im1.shape[1]])
    vInitial = np.zeros([im1.shape[0],im1.shape[1]])

	
    U = uInitial
    V = vInitial

	
    [fx, fy, ft] = computeDerivatives(im1, im2)


    for _ in range(Niter):

        uAvg = filter2(U, HSKERN)
        vAvg = filter2(V, HSKERN)

        der = (fx*uAvg + fy*vAvg + ft) / (alpha**2 + fx**2 + fy**2)

        U = uAvg - fx * der
        V = vAvg - fy * der
        U1=np.abs(U)
        V1=np.abs(V)

    return U1,V1

def computeDerivatives(im1, im2):

    fx = filter2(im1,kernelX) + filter2(im2,kernelX)
    fy = filter2(im1,kernelY) + filter2(im2,kernelY)

   # ft = im2 - im1
    ft = filter2(im1,kernelT) + filter2(im2,-kernelT)

    return fx,fy,ft

#


def buildA(img, centerX, centerY, kernelSize):
	
    mean = kernelSize//2
    count = 0
    home = img[centerY, centerX] 
    A = np.zeros([kernelSize**2, 2])
    for j in range(-mean,mean+1): 
        for i in range(-mean,mean+1): 
            if i == 0:
                Ax = 0
            else:
                Ax = (home - img[centerY+j, centerX+i])/i
            if j == 0:
                Ay = 0
            else:
                Ay = (home - img[centerY+j, centerX+i])/j
         
            A[count] = np.array([Ay, Ax])
            count += 1
  

    return A

def getPOI(xS, yS, kernelS):
    mean = kernelS // 2
    xPos = mean
    yPos = mean
    xStep = (xS-mean) // kernelS
    yStep = (yS-mean) // kernelS
    length = xStep*yStep

    poi = np.zeros((length,1,2),int)
    count = 0
    for i in range(yStep):
        for j in range(xStep):
            poi[count,0,1] = xPos
            poi[count,0,0] = yPos
            xPos += kernelS
            count += 1
        xPos = mean
        yPos += kernelS

    return poi

def buildB(imgNew, imgOld, centerX, centerY, kernelSize):
	mean = kernelSize//2
	count = 0
	home = imgNew[centerY, centerX]

	B = np.zeros([kernelSize**2])
	for j in range(-mean,mean+1):
		for i in range(-mean,mean+1):
			Bt = imgNew[centerY+j,centerX+i] - imgOld[centerY+j,centerX+i]
			B[count] = Bt
			count += 1
		# print np.linalg.norm(B)

	return B

def gaussianWeight(kernelSize, even=False):
    if even == True:
        weight = np.ones([kernelSize,kernelSize])
        weight = weight.reshape((1,kernelSize**2))
        weight = np.array(weight)[0]
        weight = np.diag(weight)
        return weight

    SIGMA = 1 #the standard deviation of your normal curve
    CORRELATION = 0 #see wiki for multivariate normal distributions
    weight = np.zeros([kernelSize,kernelSize])
    cpt = kernelSize%2 + kernelSize//2 #gets the center point
    for i in range(len(weight)):
        for j in range(len(weight)):
            ptx = i + 1
        pty = j + 1
        weight[i,j] = 1 / (2*np.pi*SIGMA**2) / (1-CORRELATION**2)**.5*np.exp(-1/(2*(1-CORRELATION**2))*((ptx-cpt)**2+(pty-cpt)**2)/(SIGMA**2))
	  
    weight = weight.reshape((1,kernelSize**2))
    weight = np.array(weight)[0] #convert to a 1D array
    weight = np.diag(weight) #convert to n**2xn**2 diagonal matrix

    return weight
	# return np.diag(weight)

def LucasKanade(im1,im2,POI,W, kernel):
# evaluate every POI
    V = np.zeros([(POI.shape)[0],2])
    for i in range(len(POI)):
        A = buildA(im2,      POI[i][0][1], POI[i][0][0], kernel)
        B = buildB(im2, im1, POI[i][0][1], POI[i][0][0], kernel)
 #solve for v
        
        Vpt = np.linalg.inv(np.dot(np.dot(np.dot(np.transpose(A),A),W**2)),np.dot(np.dot(np.transpose(A),B),W**2))
        V[i,0] = Vpt[0]
        V[i,1] = Vpt[1]

    return V
