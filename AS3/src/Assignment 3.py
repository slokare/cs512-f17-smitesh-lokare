import cv2
import numpy as np
import matplotlib.pyplot as plt
import sys
import math
import time

c=0
corners1=[]
harris_dst=[]
count=0
count2=0

cam = cv2.VideoCapture(0)
rows1=int(cam.get(4))
cols1=int(cam.get(3))
#print rows1
#print cols1
t,frame1 = cam.read()
gray1 = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
frame11=frame1
gray11=gray1

time.sleep(3)
   
rows2=int(cam.get(4))
cols2=int(cam.get(3))
#print rows2
#print cols2
t,frame2 = cam.read()
gray2 = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
frame22=frame2
gray22=gray2
cam.release()



kp1=[]
kp2=[]


def get_cam():
    global frame1
    global frame2
    global frame11
    global frame22
    global gray1
    global gray2
    global gray11
    global gray22
    global rows1
    global cols1
    global rows2
    global cols2
    global term
    global r
    global r2
    global out
    global out2
    global blk_sz2
    global gaus2    
    
    cam = cv2.VideoCapture(0)
    rows1=int(cam.get(4))
    cols1=int(cam.get(3))
    t,frame1 = cam.read()
    gray1 = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
    frame11=frame1
    gray11=gray1
    
    time.sleep(3)
    
    
    #cam = cv2.VideoCapture(0)
    rows2=int(cam.get(4))
    cols2=int(cam.get(3))
    t,frame2 = cam.read()
    gray2 = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
    frame22=frame2
    gray22=gray2
    
    ########################################initialize#####
    term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 300, 0.1)
    r = np.zeros((rows1, cols1))
    r2 = np.zeros((rows2, cols2))
    
    def outt(gray, block,gauss):
        out = cv2.cornerEigenValsAndVecs(gray, block, gauss)
        return out
    
    cv2.imshow('image1',gray1)
    cv2.imshow('image2',gray2)
    
    th = 0.22
    k = 0.21
    blk_sz=3
    gaus=3
    out=outt(gray1, blk_sz,gaus)
    kay(k)
    total_corners = thrs(th)
    gray1 = localize_draw(total_corners,gray1)
    cv2.imshow('image1',gray1)
    
    th2 = 0.22
    k2 = 0.21
    blk_sz2=3
    gaus2=3
    out2=outt(gray2, blk_sz,gaus)
    kay2(k2)
    total_corners2 = thrs2(th2)
    gray2 = localize_draw(total_corners2,gray2)
    cv2.imshow('image2',gray2)

def thrs(th):
    corners1=[]
    total_corners=[]
    global count
    global rows1
    global cols1
    global kp1
    kp1=[]
    count =0 
    print ('threshold 1',th)
    for i in range(rows1):
       for j in range(cols1):
            if (r[i][j] > th):
                
                corner = []
                corner.append(i)
                corner.append(j)
                corners1.append(corner)
                
                count = count +1
                kp1.append(cv2.KeyPoint(j,i,1))
                
    total_corners = np.float32(corners1)
    print ("corner1count 1",count)
    return total_corners


def thrs2(th):
    corners1=[]
    total_corners=[]
    global count2
    global rows2
    global cols2
    global kp2
    kp2=[]
    count2 =0 
    print ('threshold 2',th)
    #threshold=cv2.getTrackbarPos('th', 'shape')
    for i in range(rows2):
       for j in range(cols2):
            if (r2[i][j] > th):
                
                corner = []

                corner.append(i)
                corner.append(j)
                corners1.append(corner)
                
                count2 = count2 +1
                #cv2.rectangle(frame1,(j+10,i+10),(j-10,i-10),(0,255,0),1)
                kp2.append(cv2.KeyPoint(j,i,1))
               
    total_corners = np.float32(corners1)
    print ("corner1count 2",count2)
    return total_corners


def localize_draw(total_corners,gray):
    global c
    cv2.cornerSubPix(gray, total_corners, (5,5), (-1,-1), term)
    corners= np.int0(total_corners)
    gray = cv2.cvtColor(gray,cv2.COLOR_GRAY2BGR)
    c=0
    
    for corner in corners:
        x,y=corner.ravel()
            #cv2.circle(frame3,(x,y),6,255,-1)
        cv2.rectangle(gray,(y+10,x+10),(y-10,x-10),(0,255,0),1)
        cv2.putText(gray,str(c),(y-5,x+5), cv2.FONT_HERSHEY_PLAIN ,1,(0,0,255))
        c=c+1
    return gray
    
def kay(k):
    print('k',k)
    for i in range(rows1):
            for j in range(cols1):
                #r[i][j]= (abs(out[i][j][1]*abs(out[i][j][2])))+k*math.pow((abs(out[i][j][1])+abs(out[i][j][2])),2) 
                r[i][j]= (out[i][j][1]*out[i][j][2])+k*math.pow((out[i][j][1]+out[i][j][2]),2) 
    
def kay2(k2):
    print('k2',k2)
    for i in range(rows2):
            for j in range(cols2):
                #r2[i][j]= (abs(out2[i][j][1]*abs(out2[i][j][2])))+k2*math.pow((abs(out2[i][j][1])+abs(out2[i][j][2])),2) 
                r2[i][j]= (out2[i][j][1]*out2[i][j][2])+k2*math.pow((out2[i][j][1]+out2[i][j][2]),2)    


term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 300, 0.1)
r = np.zeros((rows1, cols1))
r2 = np.zeros((rows2, cols2))

def outt(gray, block,gauss):
    out = cv2.cornerEigenValsAndVecs(gray, block, gauss)
    return out

cv2.imshow('image1',gray1)
cv2.imshow('image2',gray2)

th = 0.22
k = 0.21
blk_sz=3
gaus=3
out=outt(gray1, blk_sz,gaus)
kay(k)
total_corners = thrs(th)
gray1 = localize_draw(total_corners,gray1)
cv2.imshow('image1',gray1)

th2 = 0.22
k2 = 0.21
blk_sz2=3
gaus2=3
out2=outt(gray2, blk_sz,gaus)
kay2(k2)
total_corners2 = thrs2(th2)
gray2 = localize_draw(total_corners2,gray2)
cv2.imshow('image2',gray2)

while(1):   

    key = cv2.waitKey(1)
    if key == 27: #esccape 
        break
    
    elif key == ord('o'):
        get_cam()
        
    elif key == ord('a'):

        gray1 = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
        th = th + 0.002
        total_corners = thrs(th)
        
        gray1 = localize_draw(total_corners,gray1)
        cv2.imshow('image1',gray1)          
    
    elif key == ord('z'):
        gray1 = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
        
        th = th - 0.002
        total_corners = thrs(th)
        
        gray1 = localize_draw(total_corners,gray1)
        cv2.imshow('image1',gray1)
        
    elif key == ord('s'):
        gray1 = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
        k=k+0.005
        kay(k)
        print ('K ',k)
        
        total_corners = thrs(th)
        gray1 = localize_draw(total_corners,gray1)
        cv2.imshow('image1',gray1)       
    elif key == ord('x'):
        gray1 = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
        
        k=k-0.005
        kay(k)
        print ('K ',k)
        
        total_corners = thrs(th)
        gray1 = localize_draw(total_corners,gray1)
        cv2.imshow('image1',gray1)    
    elif key == ord('d'):
        gray1 = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
        blk_sz=blk_sz+2
        out=outt(gray1, blk_sz,gaus)
        print ('block size ',blk_sz) 
    
    elif key == ord('c'):
        gray1 = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
        blk_sz=blk_sz-2
        if blk_sz>0:
            out=outt(gray1, blk_sz,gaus)
        else:
            print('min value of block size 1')
            blk_sz=1
        print ('block size ',blk_sz)    

        total_corners = thrs(th)
        gray1 = localize_draw(total_corners,gray1)
        cv2.imshow('image1',gray1) 
    
    elif key == ord('f'):
        gray1 = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
        gaus=gaus+2
        #out=outt(gray1, blk_sz,gaus)        
        print ('gaussian window 1',gaus)
        
        total_corners = thrs(th)
        gray1 = localize_draw(total_corners,gray1)
        cv2.imshow('image1',gray1)        
    elif key == ord('v'):
        gray1 = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
        gaus=gaus-2
        
        if gaus > 0:
            print('gaussian to be implemented')
            #out=outt(gray1, blk_sz,gaus)
        else:
            print('min value of gaussian window 1')
            gaus=1
        print ('gaussian window 1',gaus)
 
        total_corners = thrs(th)
        gray1 = localize_draw(total_corners,gray1)
        cv2.imshow('image1',gray1)       
#######################################################3
    elif key == ord('A'):

        gray2 = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
        
        th2 = th2 + 0.002
        total_corners2 = thrs2(th2)
        
        gray2 = localize_draw(total_corners2,gray2)
        cv2.imshow('image2',gray2)   
        
    
    elif key == ord('Z'):
        gray2 = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
        
        th2 = th2 - 0.002
        total_corners2 = thrs2(th2)
        
        gray2 = localize_draw(total_corners2,gray2)
        cv2.imshow('image2',gray2) 
        
    elif key == ord('S'):
        gray2 = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
        k2=k2+0.005
        kay2(k2)
        total_corners2 = thrs2(th2)
        
        gray2 = localize_draw(total_corners2,gray2)
        cv2.imshow('image2',gray2)       
    elif key == ord('X'):
        gray2 = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
        k2=k2-0.005
        kay2(k2)
        total_corners2 = thrs2(th2)
        
        gray2 = localize_draw(total_corners2,gray2)
        cv2.imshow('image2',gray2)    
    elif key == ord('D'):
        gray2 = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
        blk_sz=blk_sz+2
        out2=outt(gray2, blk_sz2,gaus2)
        print ('block size 2',blk_sz2) 
        total_corners2 = thrs2(th2)
        
        gray2 = localize_draw(total_corners2,gray2)
        cv2.imshow('image2',gray2)    
    elif key == ord('C'):
        gray2 = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
        blk_sz2=blk_sz2-2
        if blk_sz2>0:
            out=outt(gray2, blk_sz2,gaus2)
        else:
            print('min value of block size 1')
            blk_sz2=1
        print ('block size 2',blk_sz2)    
        total_corners2 = thrs2(th2)
        
        gray2 = localize_draw(total_corners2,gray2)
        cv2.imshow('image2',gray2)    
    elif key == ord('F'):
        gray2 = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
        gaus2=gaus2+2
        #out=outt(gray2, blk_sz2,gaus2)        
        print ('gaussian window 2',gaus2)
        total_corners2 = thrs2(th2)
        
        gray2 = localize_draw(total_corners2,gray2)
        cv2.imshow('image2',gray2)        
    elif key == ord('V'):
        gray2 = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
        gaus2=gaus2-2
        
        if gaus2 > 0:
            print('gaussian to be implemented')
            #out=outt(gray2, blk_sz2,gaus2)
        else:
            print('min value of gaussian window 1')
            gaus=1
        print ('gaussian window 2',gaus2)
        total_corners2 = thrs2(th2)
        
        gray2 = localize_draw(total_corners2,gray2)
        cv2.imshow('image2',gray2)        
    ##########################################
    elif key == ord('h'):
        print "program description:"
        print "below is the list of specific operations"
        print "For image 1:"
        print "‘a’ -> increase threshold  "
        print "'z’ -> decrease threshold"
        print "‘s’ -> increase weight of threshold"
        print "‘x’ -> decrease weight of threshold"
        print "‘d’ -> increase neighbourhood size for computing correlation matrix"
        print "‘c’ -> decrease neighbourhood size for computing correlation matrix"
        print "‘f’ -> increase variance of the gaussian(scale) "
        print "‘v’ -> decrease variance of the gaussian(scale) "
        print "For image 2:"
        print "‘A’ -> increase threshold  "
        print "‘Z’ -> decrease threshold"
        print "‘S’ -> increase weight of threshold "
        print "‘X’ -> decrease weight of threshold"
        print "‘D’ -> increase neighbourhood size for computing correlation matrix  "
        print "‘C’ -> decrease neighbourhood size for computing correlation matrix "
        print "‘F’ -> increase variance of the gaussian(scale) "
        print "‘V’ -> decrease variance of the gaussian(scale)"
        print "Other functions:"
        print "‘q’ -> Compute and plot feature vector with current parameters"
        print "‘o’ -> capture 2 new frames from camera"
        print "‘h’ -> print help menu"
        print "‘Esc’ -> Exit program"
        
    

    elif key == ord('q'):
        print('feature matching')
        orb =cv2.ORB_create()

        kp1, des1 = orb.compute(gray1, kp1)
        kp2, des2 = orb.compute(gray2, kp2)        
        
        bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
        matches = bf.match(des1,des2)
        
        matches = sorted(matches, key = lambda x:x.distance)
    
        img5 = cv2.drawMatches(gray1,kp1,gray2,kp2,matches,None,(0,0,255),flags=2)
        
        cv2.imshow('shape55',img5)

cv2.destroyAllWindows()

