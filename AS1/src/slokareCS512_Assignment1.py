
# coding: utf-8

# In[43]:

import numpy as np
from numpy.linalg import inv
from numpy import linalg as li


# In[2]:

A = np.array([1,2,3])
B = np.array([4,5,6])
C = np.array([-1,1,3])


# Answer 1.1

# In[3]:

ans = (2*A) - B
ans


# Answer 1.2

# In[11]:

a_mag  = np.sqrt((A*A).sum())
a_mag


# Answer 1.3

# In[12]:

unit_vector = A/magnitude
unit_vector


# Answer 1.4

# In[13]:

dir_cosine = A/magnitude
dir_cosine

Answer 1.5
# In[14]:

A_dot_B =np.dot(A,B)
print(A_dot_B)
B_dot_A =np.dot(B,A)
print(B_dot_A)


# Answer 1.6

# In[15]:

b_mag = np.sqrt((B*B).sum())
angle = np.dot(A,B)/a_mag/b_mag
angle = np.arccos(angle)
angle


# Answer1.7

# In[16]:

Vec =np.array([1,1,1])
perpen_vector = np.cross(A,Vec)
perpen_vector


# Answer 1.8

# In[17]:

ab_cross = np.cross(A,B)
print(ab_cross)
ba_cross = np.cross(B,A)
print(ba_cross)


# Answer 1.9

# In[19]:

per_vector_a_b = np.cross(A,B)
per_vector_a_b


# Answer 1.10

# In[21]:

determinant = np.linalg.det(np.array([[1,2,3],[4,5,6],[-1,1,3]]))
determinant


# Answer 1.11

# In[22]:

a_transpose_b =np.dot(np.transpose(A),B)
a_transpose_b


# In[24]:

b_transpose_a =np.dot(np.transpose(B),A)
b_transpose_a


# # Answer 2

# In[25]:

A = np.array([[1,2,3],[4,-2,3],[0,5,-1]])
B = np.array([[1,2,1],[2,1,-4],[3,-2,1]])
C = np.array([[1,2,3],[4,5,6],[-1,1,3]])


# In[26]:

result = (2*A) - B
result


# Answer 2.2

# In[28]:

dot_a_b = np.dot(A,B)
print(dot_a_b)
dot_b_a = np.dot(B,A)
print(dot_b_a)

Answer 2.3
# In[29]:

dot_a_b_trasnpose = np.transpose(dot_a_b)
dot_a_b_trasnpose


# In[31]:

a_transpose_b_trasnpose = np.dot(np.transpose(B),np.transpose(A))
a_transpose_b_trasnpose


# Answer 2.4

# In[32]:

a_mag= np.linalg.det(A)
a_mag


# In[33]:

c_mag= np.linalg.det(C)
c_mag


# Answer 2.5

# In[35]:

if (np.dot(A[0],A[1]) == 0 or np.dot(A[0],A[2]) == 0 or np.dot(A[1],A[2]) == 0 ):
   print("A is matrix set in whichrow are orthogonal")
if (np.dot(B[0],B[1]) == 0 or np.dot(B[0],B[2]) == 0 or np.dot(B[1],B[2]) == 0 ):      
   print("B is orthogonal matrix whichrow are orthogonalset")
if (np.dot(C[0],C[1]) == 0 or np.dot(C[0],C[2]) == 0 or np.dot(C[1],C[2]) == 0 ):
   print("C is orthogonal matrix set  whichrow are orthogonal")


# Answer 2.6

# In[38]:

a_inverse = inv(A)
a_inverse


# In[40]:

b_inverse = inv(B)
b_inverse


# # Answer3 

# In[55]:

A = np.array([[1,2],[3,2]])
B = np.array([[2,-2],[-2,5]])


# Answer 3.1

# In[65]:

value,vector = li.eig(A)


# In[66]:

vector


# In[67]:

value


# Answer 3.2

# In[68]:

value_inverse = inv(vector)
result =np.dot(value_inverse,A)
result = np.dot(result,vector)
result


# Answer 3.3

# In[69]:

result=np.dot(vector[:,0],vector[:,1])
result


# Answer 3.4

# In[72]:

b_value,b_vector = li.eig(B) 
result = np.dot(b_vector[:,0],b_vector[:,1])
result


# In[ ]:



