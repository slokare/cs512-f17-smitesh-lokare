import cv2
import numpy as np

c=0

left = cv2.imread('rock-l.tif')
right = cv2.imread('rock-r.tif')

left =cv2.cvtColor(left, cv2.COLOR_BGR2GRAY)
right =cv2.cvtColor(right, cv2.COLOR_BGR2GRAY)

rows_r,cols_r = right.shape
rows_l,cols_l = left.shape

total_left = []
total_right = []

cv2.imshow('right', right)
cv2.imshow('left', left)
        
def on_mouse_plot1(event, x, y, flags, params):
    global ix,iy,final_fundamental
    
    if event==cv2.EVENT_LBUTTONDOWN:
        ix,iy=x,y
    if event==cv2.EVENT_LBUTTONUP:
        print (ix,iy)
        a = np.array([ix,iy,1])
        d = np.matmul(a,final_fundamental.T)

        if d[0] >= d[1]:

            for x in range(0,rows_r):
                y = int((-1*d[2] -d[0]*x)/d[1])

                if y < cols_r and y > 0:
                    right[y,x] = 255

        else:
            print("else")
            for y in range(0,cols_r):
                x = int((-1*d[2] -d[1]*y)/d[0])

                if x < rows_r and x > 0:
                    right[y,x] = 255
        

        cv2.imshow('right',right)
               
        
def on_mouse_plot2(event, x, y, flags, params):
    global ix,iy,final_fundamental
    
    if event==cv2.EVENT_LBUTTONDOWN:
        ix,iy=x,y
    if event==cv2.EVENT_LBUTTONUP:
        print (ix,iy)
        a = np.array([ix,iy,1])
        d = np.matmul(a,final_fundamental)
        
  
        print (ix,iy)
        a = np.array([ix,iy,1])
        d = np.matmul(a,final_fundamental)

  
      
       
        if d[0] >= d[1]:

            for x in range(0,rows_l):
                y = int((-1*d[2] -d[0]*x)/d[1])

                if y < left.shape[1] and y > 0:
                    left[y,x] = 255

        else:
            print("else")
            for y in range(0,cols_l):
                x = int((-1*d[2] -d[1]*y)/d[0])

                if x < left.shape[0] and x > 0:
                    left[y,x] = 255
        
        cv2.imshow('left',left)
        

def on_mouse_left(event, x, y, flags, params):
    global ix,iy,c
    if event==cv2.EVENT_LBUTTONDOWN:
        ix,iy=x,y

    elif event==cv2.EVENT_LBUTTONUP:
        c=c+1
        cv2.putText(left,str(c),(ix,iy), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,255))
        xy_left = []    
        xy_left.append(ix)
        xy_left.append(iy)
        xy_left.append(1)
        total_left.append(xy_left)
        print('total left is',total_left)
        cv2.imshow('left', left)
        
def on_mouse_right(event, x, y, flags, params):
    global ix,iy,c
    if event==cv2.EVENT_LBUTTONDOWN:
        ix,iy=x,y
    elif event==cv2.EVENT_LBUTTONUP:
        c=c+1
        cv2.putText(right,str(c),(ix,iy), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,255))
        xy_right = []    
        xy_right.append(ix)
        xy_right.append(iy)
        xy_right.append(1)
        total_right.append(xy_right)
        print('total right is',total_right)
        cv2.imshow('right', right)
        
        
def M_cal(points): 
    points= np.asarray(points)       
    x_mean, y_mean,_ =  np.mean(points, axis=0) 
    x_std,y_std,z_std = np.std(points, axis=0)
    
    sig_mat = np.zeros((3,3))
    sig_mat[0,0] = 1/x_std
    sig_mat[1,1] = 1/y_std
    sig_mat[2,2] = 1
    
    mean_mat = np.zeros((3,3)) 
    np.fill_diagonal(mean_mat,1)
    mean_mat[0,2] = -x_mean
    mean_mat[1,2] = -y_mean
    return np.matmul(sig_mat,mean_mat)

def normalize(M_matrix,total_points):
    normal_points = []
    for i in total_points:
        normal_points.append(np.matmul(M_matrix,i))
    return normal_points


def build_A(norm_left,norm_right):
    size = len(norm_left)
    A =A = np.zeros((size,9),np.float32)
    index = 0
    for i,j in zip(norm_left,norm_right):
        xl,yl,z1= i
        xr,yr,z2 = j
        A[index] = xl*xr,xl*yr,xl,yl*xr,yl*yr,yl,xr,yr,1
        index =index +1
    return A
    
def start_compute(total_left,total_right):
    global normalised_left_points,normalised_right_points,final_fundamental
    print ('lets begin')
    
    left_points = np.asarray(total_left)
    right_points = np.asarray(total_right)    
#    print(left_points[:,:2])
    mat,las = cv2.findFundamentalMat(left_points[:2],right_points[:2],method =cv2.FM_8POINT)
    print("matrix",mat)
    
    M_mat = M_cal(total_left)
    norm_left = normalize(M_mat,total_left)
#    print("norm_left_point",norm_left)
    
    Mp_mat = M_cal(total_right)
    norm_right = normalize(Mp_mat,total_right)
#    print("norm_right_point",norm_right)
    
    mat_A=build_A(norm_left,norm_right)
#    print mat_A

    U, s, V = np.linalg.svd(mat_A, full_matrices=True)
    est_f = (V.T[:,-1].reshape(3,3))
    
    U_final,D,V_final= np.linalg.svd(est_f, full_matrices=True)
    D[len(D)-1] = 0
    Dp = np.diag(D)
#    print Dp
    
    f_p = np.dot(np.dot(U_final,Dp),V_final.T)
#    print("fundamental_prime",f_p )
    
    final_fundamental = np.matmul(np.matmul(M_mat.T,f_p),Mp_mat)
#    print("final_fundamental",final_fundamental)
    U,D,V =np.linalg.svd(final_fundamental, full_matrices=True)
        
    left_epipole = V.T[:,-1]
    
    left_epipole = left_epipole/left_epipole[-1]
    print ("left_epipole",left_epipole)
    
    
    right_epipole = U[:,-1]
    right_epipole = right_epipole/right_epipole[-1]
    print ("right epipole",right_epipole)
            
    cv2.putText(left,"left epipole",(int(left_epipole[0]),int(left_epipole[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255))
    cv2.putText(right,"right epipole",(int(right_epipole[0]),int(right_epipole[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255))
  
    while(1):
        cv2.imshow('right',right)
        cv2.imshow('left',left)
            
        cv2.setMouseCallback('left',on_mouse_plot1, 0)
        k = cv2.waitKey(1)
        if k == 27:
            cv2.destroyAllWindows()
            break
    
    while(1):
        cv2.imshow('right',right)
        cv2.imshow('left',left)
            
        cv2.setMouseCallback('right',on_mouse_plot2, 0)
        k = cv2.waitKey(1)
        if k == 27:
            cv2.destroyAllWindows()
            break
        
while(1):
    cv2.setMouseCallback('left',on_mouse_left, 0)
        
    key = cv2.waitKey(1)
    if key == 27: #esccape 
        break
    
    elif key == ord('h'):
        print ("program description:")
        print ("Mark more than 8 points on left image, press Esc:")
        print ("Mark same number of points on right image, press Esc:")
        print ("Click on left image to draw epipolar lines on right image. When done, press Esc:")
        print ("Click on right image to draw epipolar lines on left image. When done, press Esc:")
        print ("Note that both marking and plotting operations are sequential not parallel")
    
    
c=0  
while(1):
    cv2.setMouseCallback('right',on_mouse_right, 0)
        
    key = cv2.waitKey(1)
    if key == 27: #esccape 
        break  
    
    
cv2.destroyAllWindows()


if len(total_left) < 8  or len(total_right) < 8:
    print("Please select minimum 8 points")
    
elif len(total_left) != len(total_right):
    print("Please select equal number of points points")
else:
    start_compute(total_left,total_right)
    
    
    
cv2.destroyAllWindows()
