import numpy as np
cimport numpy as np

from libc.math cimport sqrt
from pylab import *
import matplotlib.pyplot as plt
from scipy import ndimage
from skimage.morphology import erosion, disk

ctypedef np.float64_t DTYPE_t
ctypedef np.int_t DTYPEi_t

cpdef get_patch(center_x, center_y, np.ndarray img, patch_size):

    cdef:
        int x = center_x
        int y = center_y
        int p = patch_size // 2
        np.ndarray patch = img[x-p:x+p+1,y-p:y+p+1]
    return patch

cpdef copy_patch(np.ndarray[DTYPE_t, ndim=3] patch_dst, np.ndarray[DTYPE_t, ndim=3] patch_src):

    unfilled_pixels = np.where(patch_dst[:,:,1] == 0.9999)

    cdef:

        np.ndarray[DTYPEi_t, ndim=1] unf_x = unfilled_pixels[0]

        np.ndarray[DTYPEi_t, ndim=1] unf_y = unfilled_pixels[1]
        int i = 0

    while i <= len(unf_x) - 1:
        patch_dst[unf_x[i]][unf_y[i]] = patch_src[unf_x[i]][unf_y[i]]
        i += 1

    return patch_dst

cpdef paste_patch(x, y, np.ndarray[DTYPE_t, ndim=3] patch, np.ndarray[DTYPE_t, ndim=3]img, patch_size = 9):

    cdef:
        int p = patch_size // 2
        int x0 = x-p
        int x1 = x+p
        int y0 = y-p
        int y1 = y+p
        int i,j
        int s = 0, t = 0

    for i from x0 <= i <= x1:
        for j from y0 <= j <= y1:
            img[i,j] = patch[s,t]
            t += 1
        s += 1
        t = 0

    return img

cpdef find_max_priority(np.ndarray[DTYPEi_t, ndim=1] boundary_ptx, np.ndarray[DTYPEi_t, ndim=1] boundary_pty, np.ndarray[DTYPE_t, ndim=2] confidence,
                        np.ndarray[DTYPE_t, ndim=2] dx, np.ndarray[DTYPE_t, ndim=2] dy, np.ndarray nx,
                        np.ndarray ny, patch_size, alpha = 255.0):

    cdef:
        float conf = np.sum(get_patch(boundary_ptx[0], boundary_pty[0], confidence, patch_size))/(patch_size ** 2)
        np.ndarray[DTYPE_t, ndim=2] grad = np.hypot(dx, dy)
        
        np.ndarray[DTYPE_t, ndim=2] grad_patch = abs(get_patch(boundary_ptx[0], boundary_pty[0], grad, patch_size))
    cdef:
        int xx = np.where(grad_patch == np.max(grad_patch))[0][0]
        int yy = np.where(grad_patch == np.max(grad_patch))[1][0]
        float max_gradx = dx[xx][yy]
        float max_grady = dy[xx][yy]
        float Nx = nx[boundary_ptx[0]][boundary_pty[0]]
        float Ny = ny[boundary_ptx[0]][boundary_pty[0]]

        int x = boundary_ptx[0]
        int y = boundary_pty[0]

        float data = abs(max_gradx * Nx + max_grady * Ny)

    if (Nx ** 2 + Ny ** 2) != 0:
        data /= (Nx ** 2 + Ny ** 2)

    cdef:
        float max = conf * (data / alpha)
        int i = 1
        float curr_data = 0, curr_conf = 0, curr_grad = 0

    while i < len(boundary_ptx):
        curr_patch = get_patch(boundary_ptx[i],
                               boundary_pty[i],
                               confidence,
                               patch_size)
        curr_conf = np.sum(curr_patch)/(patch_size ** 2)
        grad_patch = abs(get_patch(boundary_ptx[i], boundary_pty[i], grad, patch_size))
        xx = np.where(grad_patch == np.max(grad_patch))[0][0]
        yy = np.where(grad_patch == np.max(grad_patch))[1][0]
        max_gradx = dx[xx][yy]
        max_grady = dy[xx][yy]
        Nx = nx[boundary_ptx[i]][boundary_pty[i]]
        Ny = ny[boundary_ptx[i]][boundary_pty[i]]
        curr_data = abs(max_gradx * Nx + max_grady * Ny)
        if (Nx ** 2 + Ny ** 2) != 0:
            curr_data /= (sqrt(Nx ** 2 + Ny ** 2))
        curr_p = curr_conf * (curr_data / alpha)
        if curr_p > max:
            max = curr_p
            x = boundary_ptx[i]
            y = boundary_pty[i]
        i += 1
    return max, x, y

cpdef patch_dist(np.ndarray[DTYPE_t, ndim=3] patch_dst,
                np.ndarray[DTYPE_t, ndim=3] patch_src):

    cdef:
        int m = patch_dst.shape[0]
        int n = patch_dst.shape[1]

        np.ndarray[DTYPE_t, ndim=3] patch_srcc = patch_src[:m, :n, :]
        np.ndarray[DTYPE_t, ndim=1] patch_dst_r = patch_dst[:,:,0].flatten()
        np.ndarray[DTYPE_t, ndim=1] patch_dst_g = patch_dst[:,:,1].flatten()
        np.ndarray[DTYPE_t, ndim=1] patch_dst_b = patch_dst[:,:,2].flatten()
        np.ndarray[DTYPE_t, ndim=1] patch_src_r = patch_srcc[:,:,0].flatten()
        np.ndarray[DTYPE_t, ndim=1] patch_src_g = patch_srcc[:,:,1].flatten()
        np.ndarray[DTYPE_t, ndim=1] patch_src_b = patch_srcc[:,:,2].flatten()
        int i = 0
        int len = patch_dst_r.shape[0]
        float sum = 0

    while i <= len - 1:
        if (patch_dst_r[i] != 0.0 and
            patch_dst_g[i] != 0.9999 and
            patch_dst_b[i] != 0.0): # ignore unfilled pixels
            sum += (patch_dst_r[i] - patch_src_r[i]) ** 2
            sum += (patch_dst_g[i] - patch_src_g[i]) ** 2
            sum += (patch_dst_b[i] - patch_src_b[i]) ** 2
        i += 1
    return sum

cpdef find_exemplar_patch(np.ndarray[DTYPE_t, ndim=3] img,
                              np.ndarray[DTYPE_t, ndim=3] patch,
                              x, y, patch_size = 9):

    cdef:
        int offset = patch_size // 2
        int x_boundary = img.shape[0]
        int y_boundary = img.shape[1]
        int i = 0
        float min_dist = np.inf

        np.ndarray[DTYPE_t, ndim=3] img_copy = img[offset:x_boundary-offset+1,
                                                   offset:y_boundary-offset+1]


    filled_r = np.where(img_copy[:,:,1] != 0.9999)

    cdef:

        np.ndarray[DTYPEi_t, ndim=1] xx = filled_r[0]

        np.ndarray[DTYPEi_t, ndim=1] yy = filled_r[1]

    while i < len(xx) - 1:
        exemplar_patch = get_patch(xx[i] + offset, yy[i] + offset, img, patch_size)
        if (exemplar_patch.shape[0] == patch_size and
            exemplar_patch.shape[1] == patch_size):

            if ((xx[i] + offset) != x and (yy[i] + offset) != y
                and np.where(exemplar_patch[:,:,1] == 0.9999)[0].shape[0] == 0):
                dist = patch_dist(patch, exemplar_patch)
                if dist < min_dist:
                    best_patch = exemplar_patch
                    best_x = xx[i] + offset
                    best_y = yy[i] + offset
                    min_ssd = ssd
        i += 1
    return best_patch, best_x, best_y

cpdef update(x, y, np.ndarray confidence, np.ndarray mask, patch_size = 9):

    cdef:
        int p = patch_size // 2
        int i, j
        int x0 = x-p
        int x1 = x+p
        int y0 = y-p
        int y1 = y+p

    for i from x0 <= i <= x1:
        for j from y0 <= j <= y1:
            confidence[i,j] = 1
            mask[i,j] = 1

    return confidence, mask

cpdef inpaint(src_img, mask_img, gaussian_blur=0,
              gaussian_sigma=1, patch_size=9):


    cdef:
        np.ndarray src = src_img
        np.ndarray mask = mask_img
        np.ndarray[DTYPE_t, ndim=2] unfilled_img
        np.ndarray[DTYPE_t, ndim=2] grayscale
        np.ndarray confidence = np.zeros(mask_img.shape)
        np.ndarray dx, dy, nx, ny, fill_front
        np.ndarray [DTYPEi_t, ndim=1] boundary_ptx, boundary_pty
        int max_x, max_y, patch_count = 0
        np.ndarray[DTYPE_t, ndim=2] max_patch, copied_patch

    unfilled_img = src/255.0
    mask = mask/255.0
    grayscale = src[:,:,0]*.2125 + src[:,:,1]*.7154 + src[:,:,2]*.0721
    grayscale /= 255.0
    confidence[np.where(mask != 0)] = 1
    unfilled_img[np.where(mask == 0.0)] = [0.0, 0.9999, 0.0]

    if gaussian_blur == 1:
        grayscale = ndimage.gaussian_filter(grayscale, gaussian_sigma)

    while np.where(mask == 0)[0].any():
        fill_front = mask - erosion(mask, disk(1))

        boundary_ptx = np.where(fill_front > 0)[0] 
        boundary_pty = np.where(fill_front > 0)[1] 

        dx = ndimage.sobel(grayscale, 0)
        dy = ndimage.sobel(grayscale, 1)
        dx[np.where(mask == 0)] = 0.0
        dy[np.where(mask == 0)] = 0.0

        nx = ndimage.sobel(mask, 0)
        ny = ndimage.sobel(mask, 1)

        highest_priority = find_max_priority(boundary_ptx, boundary_pty, confidence, -dy,dx,
                                            -ny, nx, patch_size)

        max_x = highest_priority[1]
        max_y = highest_priority[2]
        max_patch = get_patch(max_x, max_y, unfilled_img, patch_size)

        best_patch = find_exemplar_patch(unfilled_img, max_patch, max_x, max_y, patch_size)
        copied_patch = copy_patch(max_patch, best_patch[0])
        unfilled_img = paste_patch(max_x, max_y, copied_patch, unfilled_img, patch_size)
        confidence_image, mask = update(max_x, max_y, confidence, mask, patch_size)
        patch_count += 1
        print (patch_count, 'patches inpainted', highest_priority[1:], '<-', best_patch[1:])
        imsave(save_name, unfilled_img)

    plt.title('Inpainted Image')
    plt.axis('off')
    plt.show(imshow(unfilled_img))
