import cv2
import sys, os, time
from exemp2 import inpaint
import numpy as np
from scipy.misc import imread

class main2():
    def __init__(self, src_image, mask_image):
        self.dirname = ''
        self.img = cv2.imread('image.jpg', 0)
        
        self.mask = cv2.imread('mask.pgm',0)
        self.patch_size = 9 
        self.gauss = 0
        self.sigma = 1
        self.drawing = False
        print 'test'
#        print self.img
 #       print self.img[0]
  #      print self.img[1]
   #     print self.img[2]


    def inpainter(self):
        
        if self.img.shape[:2] != self.mask.shape:
            print ' The source and mask image should have same dimensions'
            exit(-1)
        else:
            padded_image = np.zeros((self.img.shape[0] + self.patch_size + 1, self.img.shape[1] + self.patch_size + 1))
            padded_mask = np.ones((self.mask.shape[0] + self.patch_size + 1, self.mask.shape[1] + self.patch_size + 1))
            padded_mask[np.where(padded_mask > 0)] = 255
            dim_x = self.img.shape[0]
            dim_y = self.img.shape[1]
            print dim_x
            print dim_y
            padding_size = (self.patch_size + 1) // 2
            print padding_size
            for i in range(padding_size,dim_x):
                for j in range(padding_size,dim_y):
                    padded_image[i,j] = self.img[i,j]
                    padded_mask[i,j] = self.mask[i,j]            
            result = inpaint(padded_image, padded_mask, self.gauss, self.sigma, self.patch_size)
            return result 

    def display(self):
        cv2.imshow('Inpainted Image', result)

if __name__ == '__main__':
    test_img = main2('/Users/sushsekuboyina/PycharmProjects/pro_test/image.jpg', '/Users/sushsekuboyina/PycharmProjects/pro_test/mask.pgn')
    res = test_img.inpainter()








