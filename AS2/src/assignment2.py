import cv2
import numpy as np
import sys
import math

def nothing(x):
    pass

if len(sys.argv) == 2:
   filename = sys.argv[1]
   frame = cv2.imread(filename)
   rows,cols, ext = frame.shape
   

else:
    cam = cv2.VideoCapture(0)
    rows=cam.get(4)
    cols=cam.get(3)
    t,frame = cam.read()






#frame = cv2.imread("lena.jpg")





diagonal = math.sqrt(rows*rows+cols*cols)



def smooth(x):
    global frame
    s1 = cv2.getTrackbarPos('s','operated')
    frame_new = cv2.blur(frame_gray,(s1,s1))
    cv2.imshow('operated', frame_new)

def smooth2(x):
    global frame
    #print "Custom smoothing function"
    n = cv2.getTrackbarPos('s2','operated') 
    kernel = np.ones((n,n),np.float32)/(n*n)
    frame_new = cv2.filter2D(frame_gray,-1,kernel)
    cv2.imshow('operated', frame_new)
    

def plot():
    N = cv2.getTrackbarPos('s2','operated') 
    
    sx = cv2.Sobel(frame_gray,cv2.CV_64F,1,0,ksize=5)
    sx = cv2.normalize(sx, 0, 255, cv2.NORM_MINMAX)
         
    sy = cv2.Sobel(frame_gray,cv2.CV_64F,0,1,ksize=5)
    sy = cv2.normalize(sy, 0, 255, cv2.NORM_MINMAX)         

    for i in range(0,cols,N):
        for j in range(0,rows,N):
            nl = cv2.arrowedLine(frame_new, tuple(frame_gray[i,j]), tuple(frame_gray[i+sy,j+sx]),(0,0,255), 2)
        
    cv2.imshow('Plotted image', frame_new)
    

def rotate(x):
    global frame
    ro = cv2.getRotationMatrix2D((cols/2,rows/2),x,1)
    frame_new = cv2.warpAffine(frame_gray,ro,(cols, rows))
    #frame_new = cv2.warpAffine(frame_gray,ro,(int(diagonal),int(diagonal)))
    cv2.imshow('operated',frame_new)
    
def gray2():
    frame_gray2 = frame.copy()
    for i in range(rows):
        for j in range(cols):
            avg = int((sum(frame[i,j]))/3)
            frame_gray2[i,j] = (avg,avg,avg)
    cv2.imshow('grayscale',frame_gray2)

    
    
x=0        

cv2.namedWindow('operated')

while(1):
    
    if len(sys.argv) < 2:
        tf, frame = cam.read()

    
    frame_gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    key = cv2.waitKey(1)
    if key == 27: #esccape 
        break
    
    elif key == ord('i'):
        frame_new = frame
        
    elif key == ord('w'):
        cv2.imwrite("out.jpg", frame_new)
        
    elif key == ord('g'):
        
        cv2.imshow('grayscale', frame_gray)
        frame_new = frame_gray
        
    elif key == ord('G'):
        
        frame_new = gray2()
    
    elif key == ord('c'):
        b, g, r = cv2.split(frame)
        if x== 0:
            cv2.imshow('color_channel', b)
            x=1
        elif x == 1:
            cv2.imshow('color_channel', g)
            x=2
        elif x == 2:
            cv2.imshow('color_channel', r)
            x=0
    
    elif key == ord('s'):
        cv2.createTrackbar('s','operated',5,20,smooth)

    elif key == ord('S'):
        cv2.createTrackbar('s2','operated',5,20,smooth2)

     
    # elif own sooth fn    
        
    elif key == ord('d'):
        frame_new = cv2.pyrDown(frame)
        #frame_new = cv2.pyrUp(frame_new)
        cv2.imshow('downsampled', frame_new)

    elif key == ord('D'):
        
        frame_new = cv2.pyrDown(frame)
        frame_new = cv2.blur(frame_new,(3,3))
        cv2.imshow('downsampled', frame_new)
        
    elif key == ord('x'):
        frame_new = cv2.Sobel(frame_gray,cv2.CV_64F,1,0,ksize=5)
        frame_new = cv2.normalize(frame_new, 0, 255, cv2.NORM_MINMAX)      
        cv2.imshow('derivatives',frame_new)        
        
    elif key == ord('y'):
        frame_new = cv2.Sobel(frame_gray,cv2.CV_64F,0,1,ksize=5)
        frame_new = cv2.normalize(frame_new, 0, 255, cv2.NORM_MINMAX)      
        cv2.imshow('derivatives',frame_new)   
         
    elif key == ord('l'):
        sx = cv2.Sobel(frame_gray,cv2.CV_64F,1,0,ksize=5)
        sx = cv2.normalize(sx, 0, 255, cv2.NORM_MINMAX)
         
        sy = cv2.Sobel(frame_gray,cv2.CV_64F,0,1,ksize=5)
        sy = cv2.normalize(sy, 0, 255, cv2.NORM_MINMAX)         
        sob = np.hypot(sx, sy)
        cv2.imshow('gradient',sob)
         
    elif key == ord('r'):
        cv2.createTrackbar('ro','operated',45,360,rotate)
    elif key == ord('p'):
        cv2.createTrackbar('N','Plotted image',5,50,plot)    
        
    elif key == ord('h'):
        print "program description:"
        print "This program performs simple image manipulation"
        print "Enter 'assignment2' to run program with camera"
        print "Enter 'assignment2 [YOUR IMAGE NAME]' to run program with custom image input"
        print "This program takes following command lines"
        print "'i' -> reload original image"
        print "'w' -> save current window"
        print "'g' -> image to grayscale"
        print "'G' -> image to grayscale without inbuilt function"
        print "'s' -> To smooth the image according to slider"
        print "'S' -> To smooth the image according to slider without inbuilt function"
        print "'d' -> downasample without smoothing"
        print "'D' -> downasample with smoothing"
        print "'x' -> Convolution with x derivative"
        print "'y' -> Convolution with y derivative"
        print "'m' -> magnitude of gradient"
        print "'p' -> Plotted gradient with adjustable vector length"
        print "'r' -> Adjustable image rotation"
        print "'h' -> Diaplay Help menu"
        
        
    cv2.imshow('Original', frame)
    
cam.release()
cv2.destroyAllWindows()


  